book:
	# build index & PDF with LaTeX
	pdflatex book.tex
	makeindex book.idx
	pdflatex book.tex

	# open PDF in default reader
	xdg-open book.pdf

backup:
	# backup working copy to Amazon S3
	s3cmd sync -v -r ./* s3://white-paper/LaTeXBookTemplate/
	s3cmd ls s3://white-paper/LaTeXBookTemplate/

tidy:
	# remove temporary files
	rm -f *.out *.pdf *.aux *.dvi *.log *.blg *.bbl *.tex-e
	rm -f *.idx *.ind *.ilg *.toc *.bak *.txt
	ls -l

detex:
	cat ch00.tex ch01.tex ch02.tex ch09.tex > plain.tex
	detex < plain.tex > _plain.txt
	fold -s -w 78 < _plain.txt > plain.txt
	rm plain.tex _plain.txt
	wc -w plain.txt

spell:
	find ch*.tex -exec aspell --lang=en_GB -t -c {} \;